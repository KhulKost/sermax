#include <iostream>

#include <netdb.h>
#include <unistd.h>
#include <string.h>

#include "parser.h"

using namespace std;

using socklen_t = unsigned int;

int main()
{
	char buf[256];
	int sockfd = 0;
	struct sockaddr_in listen_addr;
	int optval = 1;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		cout << "ERROR:(func socket)\n" << strerror(errno) << "\n";
		return -1;
	}

	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, ( const void *)&optval, sizeof(socklen_t)))
	{
		cout << "ERROR:(func setsockopt)\n" << strerror(errno) << "\n";
		return -1;
	}

	listen_addr.sin_family = AF_INET;
	listen_addr.sin_addr.s_addr = INADDR_ANY;
	listen_addr.sin_port = htons(8080);

	if (bind(sockfd, (struct sockaddr *)&listen_addr, sizeof(listen_addr)) == -1)
	{
		cout << "ERROR:(func bind)\n" << strerror(errno) << "\n";
		return -1;
	}

	if (listen(sockfd, 100) == -1)
	{
		cout << "ERROR:(func listen)\n" << strerror(errno) << "\n";
		return -1;
	}

	for (;;)
	{
		int accepted_socket = 0;
		socklen_t *addrlen = nullptr;
		try
		{
			addrlen = new socklen_t();
		}
		catch (std::bad_alloc&)
		{
			cout << "Error: No memory\n";
			return -1;
		}

		*addrlen = sizeof(listen_addr);

		accepted_socket = accept(sockfd, (struct sockaddr *)&listen_addr, addrlen);

		if (accepted_socket == -1)
		{
			cout << "ERROR:(func accept)\n" << strerror(errno) << "\n";
			return -1;
		}
		memset(buf, 0, 256);
		if (read(accepted_socket, buf, 100) == -1)
		{
			cout << "ERROR:(func read)\n" << strerror(errno) << "\n";
			return -1;
		}

		string strbuf(buf);
		HttpRequest method = parse_request(strbuf);

		if (write(accepted_socket, buf, 100) == -1)
		{
			cout << "ERROR:(func write)\n" << strerror(errno) << "\n";
			return -1;
		}

		delete addrlen;
	}

	close(sockfd);

	return 0;
}
