#include <string>

using namespace std;

class HttpRequest
{
  string method;
  string path;
  string error;
public:
  HttpRequest()
  {
    this->method = "";
    this->path = "";
    this->error = "";
  }

  HttpRequest(string method, string path)
  {
    this->method = method;
    this->path = path;
  }

  HttpRequest(string error)
  {
    this->error = error;
  }
};

HttpRequest parse_request(string buf);
