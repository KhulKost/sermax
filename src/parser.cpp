#include <iostream>
#include <string>
#include <vector>
#include <ctype.h>

#include "parser.h"

using namespace std;

#define METHOD 0;
#define PATH 1;

HttpRequest parse_request(string buf)
{
  string method;
  string path;
  string::iterator it = buf.begin();
  int state = 0;

  while (it != buf.end())
  {
          switch (state)
          {
          case METHOD:
                  if (isspace(*it))
                  {
                          state = PATH;
                          break;
                  }
                  method += *it;
                  break;
          case PATH:
                  if (isspace(*it))
                          break;
                  path += *it;
                  break;
          }
          it++;
  }

  for (auto &s : method)
          s = toupper(s);

  if (method != "GET" || method != "POST" || method != "HEAD" ||
      method != "CONNECT" || method != "OPTION" || method != "PUT"
      || method != "PATCH")
      {

        return HttpRequest("405 Method Not Allowed");
      }

  return HttpRequest("", "");
}
