all:
	g++ -c src/parser.cpp -o src/parser.o
	g++ -c src/server.cpp -o src/server.o
	g++ src/parser.o src/server.o -o src/server

clean:
	rm -rf src/parser.o
	rm -rf src/server.o
